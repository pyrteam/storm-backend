<?php

namespace App\Core\Loss;

use App\Core\Functions;

abstract class LossFunctions extends Functions
{
    const LOSS_FUNCTION_SQUARE = 'meanSquare';

    protected function register()
    {
        $this->addFunction(self::LOSS_FUNCTION_SQUARE, [$this, 'meanSquare']);
    }

    public function meanSquare(float $some)
    {
        return $some * 2;
    }
}