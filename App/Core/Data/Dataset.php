<?php

namespace App\Core\Data;

class Dataset
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * @var array
     */
    private $inputs = [];

    /**
     * @var array
     */
    private $outputs = [];

    /**
     * @var array
     */
    private $inputTests = [];

    /**
     * @var array
     */
    private $outputTests = [];

    public function setData(string $file)
    {
        if (!file_exists($file))
        {
            throw new DataException('File '.$file.' does not exist.');
        }

        switch (explode('.', $file)[1])
        {
            case 'csv':
                $this->readFromCsv($file);
                break;
            default:
                throw new DataException('File extension not supported.');
        }

        return $this;
    }

    private function readFromCsv(string $file)
    {
        $this->data = array_map('str_getcsv', file($file));
        array_shift($this->data);
    }

    public function shuffleData()
    {
        shuffle($this->data);

        return $this;
    }

    public function setInputs($startIndex, int $endIndex = -1)
    {
        $this->inputs = $this->retrieveDataFromIndexes($startIndex, $endIndex);

        return $this;
    }

    public function setOutputs($startIndex, int $endIndex = -1)
    {
        $this->outputs = $this->retrieveDataFromIndexes($startIndex, $endIndex);

        return $this;
    }

    private function retrieveDataFromIndexes($startIndex, int $endIndex = -1)
    {
        if ($endIndex == -1)
        {
            $endIndex = $startIndex;
        }

        $indexArray = [];
        if (!is_array($startIndex))
        {
            for ($i = $startIndex; $i < $endIndex + 1; $i++)
            {
                $indexArray[] = $i;
            }
        }
        else
        {
            $indexArray = $startIndex;
        }

        $results = [];
        foreach ($this->data as $row)
        {
            $inputRow = [];
            foreach ($indexArray as $index)
            {
                $inputRow[] = $row[$index];
            }
            $results[] = $inputRow;
        }

        return $results;
    }

    public function data()
    {
        return $this->data;
    }

    public function inputs()
    {
        return $this->inputs;
    }

    public function outputs()
    {
        return $this->outputs;
    }

    public function inputTests()
    {
        return $this->inputTests;
    }

    public function testOutputs()
    {
        return $this->outputTests;
    }

    public function encodeInputTextToInteger($indexes)
    {
        if (!is_array($indexes))
        {
            $indexes = [$indexes];
        }

        foreach ($indexes as $index)
        {
            $stringMap = [];
            foreach ($this->inputs as &$input)
            {
                $value = array_search($input[$index], $stringMap);
                if ($value === false)
                {
                    $stringMap[] = $input[$index];
                    $value = sizeof($stringMap) - 1;
                }

                $input[$index] = $value;
            }
        }

        return $this;
    }

    public function convertToDummyVariables($indexes)
    {
        if (!is_array($indexes))
        {
            $indexes = [$indexes];
        }

        foreach ($indexes as $index)
        {
            $this->convertColumnToDummyVariables($index);
        }

        return $this;
    }

    private function convertColumnToDummyVariables($index)
    {
        $max = 0;
        foreach ($this->inputs as $input)
        {
            if ($input[$index] > $max)
            {
                $max = $input[$index];
            }
        }

        $numDigits = strlen(decbin($max));
        foreach ($this->inputs as &$input)
        {
            $dummyRow = [];
            for ($i = 0; $i < $numDigits; $i++)
            {
                $dummyRow[] = 0;
            }

            $valueInBinary = decbin($input[$index]);
            $valueInBinaryArray = array_reverse(str_split($valueInBinary));
            for ($i = 0; $i < sizeof($valueInBinaryArray); $i++)
            {
                $dummyRow[$numDigits - 1 - $i] = $valueInBinaryArray[$i];
            }

            array_splice($input, $index, 1);
            $input = array_merge($dummyRow, $input);
        }

        return $this;
    }

    public function extract_test_samples(int $testPercentage)
    {
        $numInputTestElements = ceil(sizeof($this->inputs) * $testPercentage / 100);
        $numOutputTestElements = ceil(sizeof($this->outputs) * $testPercentage / 100);

        $this->inputTests = array_splice($this->inputs, 0, $numInputTestElements);
        $this->outputTests = array_splice($this->outputs, 0, $numOutputTestElements);

        return $this;
    }

    public function scaleAll()
    {
        for ($i = 0; $i < sizeof($this->inputs[0]); $i++)
        {
            $min = 9999999999;
            $max = -999999999;
            foreach ($this->inputs as $input)
            {
                if ($input[$i] < $min)
                {
                    $min = $input[$i];
                }

                if ($input[$i] > $max)
                {
                    $max = $input[$i];
                }
            }

            $maxMinusMin = $max - $min;
            foreach ($this->inputs as &$input)
            {
                $input[$i] = ($input[$i] - $min) / $maxMinusMin;
            }
        }

        return $this;
    }
}