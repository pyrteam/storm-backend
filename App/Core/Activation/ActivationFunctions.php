<?php

namespace App\Core\Activation;

use App\Core\Functions;

abstract class ActivationFunctions extends Functions
{
    const NONE = 'none';
    const SIGMOID = 'sigmoid';
    const RECTIFIER = 'rectifier';

    protected function register()
    {
        $this->addFunction(self::NONE, [$this, 'none']);
        $this->addFunction(self::SIGMOID, [$this, 'sigmoid']);
        $this->addFunction(self::RECTIFIER, [$this, 'rectifier']);
    }

    public function rectifier(float $x)
    {
        return max($x, 0);
    }

    public function rectifierDerivative(float $x)
    {
        return $x > 0.0 ? 1 : 0;
    }

    public function sigmoid(float $x)
    {
        return 1 / (1 + pow(M_E, -$x));
    }

    public function sigmoidDerivative(float $x)
    {
        return $this->sigmoid($x) * (1 - $this->sigmoid($x));
    }

    public function none(float $x)
    {
        return $x;
    }

    public function noneDerivative(float $x)
    {
        return $x;
    }
}