<?php

namespace App\Core;

abstract class Functions
{
    /**
     * @var array
     */
    private $functions = [];

    public function __construct()
    {
        $this->register();
    }

    abstract protected function register();

    protected function addFunction(string $name, $function)
    {
        $this->functions[$name] = $function;
        $this->functions[$name.'Derivative'] = [$function[0], $function[1].'Derivative'];
    }

    public function findClosure(string $name)
    {
        if (!isset($this->functions[$name]))
        {
            return null;
        }

        return function(float $x) use ($name)
        {
            return $this->$name($x);
        };
    }
}