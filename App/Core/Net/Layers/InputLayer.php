<?php

namespace App\Core\Net\Layers;

use App\Core\Net\Nodes\Node;
use Closure;

class InputLayer extends Layer
{
    public function __construct(int $numNodes, Closure $activationFunction)
    {
        parent::__construct($numNodes, Node::INPUT_NODE, $activationFunction, $activationFunction, null);
    }

    public function setInputValues(array $values)
    {
        foreach ($this->nodes() as $index => &$node)
        {
            $node->setOutput($values[$index]);
        }
    }
}