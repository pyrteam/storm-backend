<?php

namespace App\Core\Net\Layers;

use App\Core\Net\Nodes\Node;
use Closure;

class HiddenLayer extends Layer
{
    public function __construct(int $numNodes, Closure $activationFunction, Closure $activationFunctionDerivative, $previousLayer)
    {
        parent::__construct($numNodes, Node::HIDDEN_NODE, $activationFunction, $activationFunctionDerivative, $previousLayer);
    }

    public function computeHiddenDeltas()
    {
        foreach ($this->nodes as $nodeIndex => $node)
        {
            $node->computeDelta();
        }
    }
}