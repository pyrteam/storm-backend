<?php

namespace App\Core\Net\Layers;

use App\Core\Net\Nodes\HiddenNode;
use App\Core\Net\Nodes\InputNode;
use App\Core\Net\Nodes\Node;
use App\Core\Net\Nodes\OutputNode;
use Closure;

abstract class Layer
{
    /**
     * @var array
     */
    protected $nodes = [];

    public function __construct(int $numNodes, string $nodeType, Closure $activationFunction, Closure $activationFunctionDerivative, $previousLayer)
    {
        $this->populateNodes($numNodes, $nodeType, $activationFunction, $activationFunctionDerivative, $previousLayer);
    }

    private function populateNodes(int $numNodes, string $nodeType, Closure $activationFunction, Closure $activationFunctionDerivative, $previousLayer)
    {
        switch ($nodeType)
        {
            case Node::OUTPUT_NODE:
                for ($i = 0; $i < $numNodes; $i++)
                {
                    $this->nodes[] = new OutputNode($i, $previousLayer, $activationFunction, $activationFunctionDerivative);
                }
                return;
            case Node::INPUT_NODE:
                for ($i = 0; $i < $numNodes; $i++)
                {
                    $this->nodes[] = new InputNode($i, $previousLayer, $activationFunction, $activationFunctionDerivative);
                }
                return;
            case Node::HIDDEN_NODE:
                for ($i = 0; $i < $numNodes; $i++)
                {
                    $this->nodes[] = new HiddenNode($i, $previousLayer, $activationFunction, $activationFunctionDerivative);
                }
                return;
        }
    }

    public function randomizeWeights(float $min, float $max)
    {
        foreach ($this->nodes as $node)
        {
            $node->randomizeWeights($min, $max);
        }
    }

    public function setWeights(float $value)
    {
        foreach ($this->nodes as $node)
        {
            $node->setWeights($value);
        }
    }

    public function randomizeBiases(float $min, float $max)
    {
        foreach ($this->nodes as $node)
        {
            $node->randomizeBias($min, $max);
        }
    }

    public function setBiases(float $value)
    {
        foreach ($this->nodes as $node)
        {
            $node->setBias($value);
        }
    }

    public function setNextLayer(Layer $nextLayer)
    {
        foreach ($this->nodes as $node)
        {
            $node->setNextLayer($nextLayer);
        }
    }

    public function size()
    {
        return sizeof($this->nodes);
    }

    public function nodes()
    {
        return $this->nodes;
    }

    public function computeNodeOutputs()
    {
        foreach ($this->nodes as $node)
        {
            $node->computeOutput();
        }
    }

    public function updateNodes(float $learningRate)
    {
        foreach ($this->nodes as $node)
        {
            $node->update($learningRate);
        }
    }
}