<?php

namespace App\Core\Net\Layers;

use App\Core\Net\Nodes\Node;
use Closure;

class OutputLayer extends Layer
{
    public function __construct(int $numNodes, Closure $activationFunction, Closure $activationFunctionDerivative, $previousLayer)
    {
        parent::__construct($numNodes, Node::OUTPUT_NODE, $activationFunction, $activationFunctionDerivative, $previousLayer);
    }

    public function results()
    {
        $results = [];
        foreach ($this->nodes() as $node)
        {
            $results[] = $node->output();
        }

        return $results;
    }

    public function computeOutputDeltas(array $results)
    {
        foreach ($this->nodes as $nodeIndex => $node)
        {
            $node->computeDelta($results[$nodeIndex]);
        }
    }
}