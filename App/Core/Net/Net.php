<?php

namespace App\Core\Net;

use App\Core\Net\Layers\HiddenLayer;
use App\Core\Net\Layers\InputLayer;
use App\Core\Net\Layers\OutputLayer;
use Config\ActivationExtension;

class Net
{
    /**
     * @var array
     */
    private $layers = [];

    /**
     * @var InputLayer
     */
    private $inputLayer;

    /**
     * @var OutputLayer
     */
    private $outputLayer;

    /**
     * @var ActivationExtension
     */
    private $activationFunctions;

    public function __construct()
    {
        $this->activationFunctions = new ActivationExtension();
    }

    public function addInputLayer(int $numNodes)
    {
        $this->inputLayer = new InputLayer($numNodes, $this->activationFunctions->findClosure(ActivationExtension::NONE));

        return $this;
    }

    public function addOutputLayer(int $numRows, string $activationFunction)
    {
        $this->outputLayer = new OutputLayer($numRows, $this->activationFunctions->findClosure($activationFunction),
            $this->activationFunctions->findClosure($activationFunction.'Derivative'), $this->layers[sizeof($this->layers) - 1]);

        $this->layers[sizeof($this->layers) - 1]->setNextLayer($this->outputLayer);

        return $this;
    }

    public function addLayer(int $numNodes, string $activationFunction)
    {
        $previousLayer = $this->inputLayer;
        if (sizeof($this->layers) > 0)
        {
            $previousLayer = $this->layers[sizeof($this->layers) - 1];
        }

        $this->layers[] = new HiddenLayer($numNodes, $this->activationFunctions->findClosure($activationFunction),
            $this->activationFunctions->findClosure($activationFunction.'Derivative'), $previousLayer);

        if (sizeof($this->layers) == 1)
        {
            $this->inputLayer->setNextLayer($this->layers[0]);
        }
        else
        {
            $this->layers[sizeof($this->layers) - 2]->setNextLayer($this->layers[sizeof($this->layers) - 1]);
        }

        return $this;
    }

    public function randomizeWeights(float $min = 0.0, float $max = 1.0)
    {
        foreach ($this->layers as $layer)
        {
            $layer->randomizeWeights($min, $max);
        }

        $this->outputLayer->randomizeWeights($min, $max);

        return $this;
    }

    public function setWeights(float $value)
    {
        foreach ($this->layers as $layer)
        {
            $layer->setWeights($value);
        }

        $this->outputLayer->setWeights($value);

        return $this;
    }

    public function randomizeBiases(float $min = 0.0, float $max = 1.0)
    {
        foreach ($this->layers as $layer)
        {
            $layer->randomizeBiases($min, $max);
        }

        $this->outputLayer->randomizeWeights($min, $max);

        return $this;
    }

    public function setBiases(float $value)
    {
        foreach ($this->layers as $layer)
        {
            $layer->setBiases($value);
        }

        $this->outputLayer->setBiases($value);

        return $this;
    }

    public function inputLayer()
    {
        return $this->inputLayer;
    }

    public function outputLayer()
    {
        return $this->outputLayer;
    }

    public function layers()
    {
        return $this->layers;
    }
}