<?php

namespace App\Core\Net\Nodes;

use Closure;

class OutputNode extends Node
{
    public function __construct(int $index, $previousLayer, Closure $activationFunction, Closure $activationFunctionDerivative)
    {
        parent::__construct(self::OUTPUT_NODE, $index, $previousLayer, $activationFunction, $activationFunctionDerivative);
    }

    public function computeDelta(float $expected)
    {
        $this->delta += ($this->output - $expected) * ($this->activationFunctionDerivative)($this->output);
    }
}