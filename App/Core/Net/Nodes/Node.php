<?php

namespace App\Core\Net\Nodes;

use App\Core\Net\Layers\Layer;
use Closure;

abstract class Node
{
    protected const INITIAL_BIAS_VALUE = 0.1;

    public const INPUT_NODE = 'INPUT_NODE';
    public const OUTPUT_NODE = 'OUTPUT_NODE';
    public const HIDDEN_NODE = 'HIDDEN_NODE';

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    protected $index;

    /**
     * @var Layer
     */
    protected $previousLayer;

    /**
     * @var Layer
     */
    protected $nextLayer;

    /**
     * @var array
     */
    protected $outputNodes = [];

    /**
     * @var array
     */
    protected $weights;

    /**
     * @var float
     */
    protected $bias;

    /**
     * @var Closure
     */
    protected $activationFunction;

    /**
     * @var Closure
     */
    protected $activationFunctionDerivative;

    /**
     * @var float
     */
    protected $output;

    /**
     * @var float
     */
    protected $delta;

    public function __construct(string $type, int $index, $previousLayer, Closure $activationFunction, Closure $activationFunctionDerivative)
    {
        $this->type = $type;
        $this->index = $index;
        $this->previousLayer = $previousLayer;
        $this->activationFunction = $activationFunction;
        $this->activationFunctionDerivative = $activationFunctionDerivative;

        $this->initValues();
    }

    private function initValues()
    {
        if (!is_null($this->previousLayer))
        {
            for ($i = 0; $i < $this->previousLayer->size(); $i++)
            {
                $this->weights[] = mt_rand() / mt_getrandmax();
            }
        }

        $this->bias = self::INITIAL_BIAS_VALUE;

        $this->delta = 0.0;
    }

    public function randomizeWeights(float $min, float $max)
    {
        foreach ($this->weights as &$weight)
        {
            $weight = mt_rand() / mt_getrandmax();
        }
    }

    public function setWeights(float $value)
    {
        foreach ($this->weights as &$weight)
        {
            $weight = $value;
        }
    }

    public function randomizeBias(float $min, float $max)
    {
        $this->bias = mt_rand() / mt_getrandmax();
    }

    public function setBias(float $value)
    {
        $this->bias = $value;
    }

    public function setNextLayer(Layer $nextLayer)
    {
        $this->nextLayer = $nextLayer;
    }

    public function weightAt(int $index)
    {
        return array_key_exists($index, $this->weights) ? $this->weights[$index] : 0.0;
    }

    public function setOutput(float $value)
    {
        $this->output = $value;
    }

    public function output()
    {
        return $this->output;
    }

    public function delta()
    {
        return $this->delta;
    }

    public function bias()
    {
        return $this->bias;
    }

    public function computeOutput()
    {
        $sum = $this->sumConnections();

        $this->output = $this->activate($sum);
    }

    private function sumConnections()
    {
        if (is_null($this->previousLayer))
        {
            return 0.0 + $this->bias;
        }

        $sum = 0.0;
        foreach ($this->previousLayer->nodes() as $index => $node)
        {
            $sum += $node->output() * $this->weights[$index];
        }

        return $sum + $this->bias;
    }

    private function activate(float $sum)
    {
        return ($this->activationFunction)($sum);
    }

    public function update(float $learningRate)
    {
        $this->updateWeights($learningRate);
        $this->updateBias($learningRate);

        $this->delta = 0.0;
    }

    private function updateWeights(float $learningRate)
    {
        if (is_null($this->previousLayer))
        {
            return;
        }

        foreach ($this->previousLayer->nodes() as $index => $node)
        {
            $this->weights[$index] += -$learningRate * $this->delta * $node->output();
        }
    }

    private function updateBias(float $learningRate)
    {
        $this->bias += -$learningRate * $this->delta;
    }
}