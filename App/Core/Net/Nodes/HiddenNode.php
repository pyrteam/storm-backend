<?php

namespace App\Core\Net\Nodes;

use Closure;

class HiddenNode extends Node
{
    public function __construct(int $index, $previousLayer, Closure $activationFunction, Closure $activationFunctionDerivative)
    {
        parent::__construct(self::HIDDEN_NODE, $index, $previousLayer, $activationFunction, $activationFunctionDerivative);
    }

    public function computeDelta()
    {
        $sum = 0.0;
        foreach ($this->nextLayer->nodes() as $node)
        {
            $sum += ($node->delta() * $node->weightAt($this->index));
        }

        $this->delta += ($this->activationFunctionDerivative)($this->output) * $sum;
    }
}