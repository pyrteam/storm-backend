<?php

namespace App\Core\Net\Nodes;

use Closure;

class InputNode extends Node
{
    public function __construct(int $index, $previousLayer, Closure $activationFunction, Closure $activationFunctionDerivative)
    {
        parent::__construct(self::INPUT_NODE, $index, $previousLayer, $activationFunction, $activationFunctionDerivative);
    }
}