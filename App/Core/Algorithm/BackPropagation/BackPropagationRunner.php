<?php

namespace App\Core\Algorithm\BackPropagation;

use App\Core\Algorithm\AlgorithmRunner;
use App\Core\Algorithm\FeedForward\FeedForwardRunner;
use App\Core\Data\Dataset;
use App\Core\Net\Net;
use Closure;
use Config\LossExtensions;

class BackPropagationRunner extends AlgorithmRunner
{
    /**
     * @var Net
     */
    private $net;

    /**
     * @var Dataset
     */
    private $dataset;

    /**
     * @var FeedForwardRunner
     */
    private $feedForwardAlgorithm;

    /**
     * @var Closure
     */
    private $lossFunction;

    /**
     * @var float
     */
    private $learningRate;

    /**
     * @var int
     */
    private $batchSize;

    /**
     * @var int
     */
    private $epochs;

    public function __construct(Net $net, Dataset $dataset, string $lossFunction, float $learningRate, int $batchSize, int $epochs)
    {
        $this->net = $net;
        $this->dataset = $dataset;
        $this->learningRate = $learningRate;
        $this->batchSize = $batchSize;
        $this->epochs = $epochs;

        $lossFunctions = new LossExtensions();
        $this->lossFunction = $lossFunctions->findClosure($lossFunction);

        $this->feedForwardAlgorithm = new FeedForwardRunner($net, $dataset);
    }

    public function run()
    {
        for ($i = 0; $i < $this->epochs; $i++)
        {
            echo 'Epoch '.($i + 1).'/'.$this->epochs.PHP_EOL;

            $this->runEpoch($this->batchSize, $this->learningRate);
            $this->runTest();
        }
    }

    private function runTest()
    {
        $accuracy = $this->feedForwardAlgorithm->runTest();

        echo '    Accuracy '.$accuracy.'%'.PHP_EOL;
    }

    private function runEpoch(int $batchSize, float $learningRate)
    {
        $numBatches = ceil(sizeof($this->dataset->inputs()) / $batchSize);
        $epochError = 0.0;
        for ($i = 0; $i < $numBatches; $i++)
        {
            $percent = $i * 100 / $numBatches;
            echo 'Batch '.($i + 1).'/'.$numBatches.' ['.str_repeat('=', $percent).str_repeat('.', 100 - $percent).']';

            $error = $this->runBatch($i * $batchSize, $batchSize);
            $epochError += $error;

            $this->update($learningRate);

            echo "\r";
        }

        $epochError /= $numBatches;

        echo PHP_EOL.'Error '.$epochError;
    }

    private function runBatch(int $offset, int $batchSize)
    {
        $error = 0.0;
        for ($i = 0; $i < $batchSize; $i++)
        {
            if (sizeof($this->dataset->inputs()) <= $offset + $i)
            {
                break;
            }

            $error += $this->runSample($offset + $i);
        }

        return $error / 2.0;
    }

    private function runSample(int $offset)
    {
        $results = $this->feedForwardAlgorithm->run($offset);

        $this->computeDeltas($offset);

        return $this->computeError($offset, $results);
    }

    private function computeError(int $dataOffset, array $results)
    {
        $sum = 0.0;
        foreach ($results as $resultIndex => $result) {
            $diff = $result - $this->dataset->outputs()[$dataOffset][$resultIndex];
            $diff = pow($diff, 2);
            $sum += $diff;
        }

        return $sum / sizeof($results);
    }

    private function computeDeltas(int $dataOffset)
    {
        $this->net->outputLayer()->computeOutputDeltas($this->dataset->outputs()[$dataOffset]);

        for ($i = sizeof($this->net->layers()) - 1; $i >= 0; $i--)
        {
            $this->net->layers()[$i]->computeHiddenDeltas();
        }
    }

    private function update(float $learningRate)
    {
        $this->net->outputLayer()->updateNodes($learningRate);

        for ($i = sizeof($this->net->layers()) - 1; $i >= 0; $i--)
        {
            $this->net->layers()[$i]->updateNodes($learningRate);
        }
    }
}