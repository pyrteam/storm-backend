<?php

namespace App\Core\Algorithm\FeedForward;

use App\Core\Algorithm\AlgorithmRunner;
use App\Core\Data\Dataset;
use App\Core\Net\Net;

class FeedForwardRunner extends AlgorithmRunner
{
    /**
     * @var Net
     */
    private $net;

    /**
     * @var Dataset
     */
    private $dataset;

    public function __construct(Net $net, Dataset $dataset)
    {
        $this->net = $net;
        $this->dataset = $dataset;
    }

    public function run(int $offset = 0)
    {
        $this->net->inputLayer()->setInputValues($this->dataset->inputs()[$offset]);

        foreach ($this->net->layers() as $layer)
        {
            $layer->computeNodeOutputs();
        }

        $this->net->outputLayer()->computeNodeOutputs();

        return $this->net->outputLayer()->results();
    }

    public function runTest(float $toleranceMargin = 0.01)
    {
        if (sizeof($this->dataset->inputTests()) == 0)
        {
            return 0;
        }

        $accuracy = 0;
        foreach ($this->dataset->inputTests() as $inputIndex => $inputTest)
        {
            $this->net->inputLayer()->setInputValues($inputTest);

            foreach ($this->net->layers() as $layer)
            {
                $layer->computeNodeOutputs();
            }

            $this->net->outputLayer()->computeNodeOutputs();

            $correctCount = 0;
            foreach ($this->net->outputLayer()->results() as $resultIndex => $result)
            {
                $expected = $this->dataset->testOutputs()[$inputIndex][$resultIndex];
                if ($result >= $expected - $toleranceMargin && $result <= $expected + $toleranceMargin)
                {
                    $correctCount++;
                }
            }

            $accuracy += $correctCount * 100 / sizeof($this->net->outputLayer()->results());
        }

        return $accuracy / sizeof($this->dataset->inputTests());
    }
}