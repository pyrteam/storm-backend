<?php

namespace App\Core;

use App\Core\Algorithm\BackPropagation\BackPropagationRunner;
use App\Core\Algorithm\FeedForward\FeedForwardRunner;
use App\Core\Net\Net;
use App\Core\Data\Dataset;

class NeuralNet
{
    /**
     * @var Dataset
     */
    private $dataset;

    /**
     * @var Net
     */
    private $net;

    public function __construct()
    {
        $this->dataset = new Dataset();
    }

    public function setData(string $file)
    {
        return $this->dataset->setData($file);
    }

    public function build()
    {
        $this->net = new Net();

        return $this->net;
    }

    public function run()
    {
        $algorithm = new FeedForwardRunner($this->net, $this->dataset);

        return $algorithm->run();
    }

    public function train(string $lossFunction, float $learningRate, int $batchSize, int $epochs)
    {
        $algorithm = new BackPropagationRunner($this->net, $this->dataset, $lossFunction, $learningRate, $batchSize, $epochs);

        return $algorithm->run();
    }
}