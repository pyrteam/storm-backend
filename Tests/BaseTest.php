<?php

namespace Tests;

use App\Core\Activation\ActivationFunctions;
use App\Core\Data\Dataset;
use App\Core\Net\Net;
use Config\ActivationExtension;
use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase
{
    /**
     * @var Net
     */
    protected $net;

    /**
     * @var Dataset
     */
    protected $dataset;

    /**
     * @var ActivationFunctions
     */
    protected $activationFunctions;

    protected function basePath()
    {
        return realpath(__DIR__.'/..');
    }

    public function setUp()
    {
        parent::setUp();

        $this->setUpNet();
        $this->setUpDataset();
        $this->setUpActivationFunctions();
    }

    protected function setUpNet()
    {
        $this->net = new Net();
    }

    protected function setUpDataset()
    {
        $this->dataset = new Dataset();
    }

    protected function setUpActivationFunctions()
    {
        $this->activationFunctions = new ActivationExtension();
    }

    protected function loadSimpleDataset()
    {
        $this->dataset->setData($this->basePath().'/Tests/Data/simple.csv')
            ->setInputs(0, 1)
            ->setOutputs(2);
    }

    protected function loadAccuracyDataset100()
    {
        $this->dataset->setData($this->basePath().'/Tests/Data/accuracy100.csv')
            ->setInputs(0, 1)
            ->setOutputs(2);
    }

    protected function loadAccuracyDataset50()
    {
        $this->dataset->setData($this->basePath().'/Tests/Data/accuracy50.csv')
            ->setInputs(0, 1)
            ->setOutputs(2);
    }
}