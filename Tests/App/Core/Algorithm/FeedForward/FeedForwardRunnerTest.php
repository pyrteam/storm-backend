<?php

namespace Tests\App\Core\Algorithm\FeedForward;

use App\Core\Activation\ActivationFunctions;
use App\Core\Algorithm\FeedForward\FeedForwardRunner;
use Tests\BaseTest;

class FeedForwardRunnerTest extends BaseTest
{

    /**
     * @var FeedForwardRunner
     */
    private $algorithm;

    public function setUp()
    {
        parent::setUp();

        $this->algorithm = new FeedForwardRunner($this->net, $this->dataset);
    }

    public function test_run_withOnlyRectifier()
    {
        // Arrange
        $this->loadSimpleDataset();
        $this->net->addInputLayer(1)
            ->addLayer(1, ActivationFunctions::RECTIFIER)
            ->addOutputLayer(1, ActivationFunctions::RECTIFIER)
            ->setWeights(0.1)
            ->setBiases(0.1);

        // Act
        $results = $this->algorithm->run();

        // Assert
        $this->assertEquals(1, sizeof($results));
        $this->assertEquals(0.114, $results[0]);
    }

    public function test_run_withOnlyRectifierAndTwoInputs()
    {
        // Arrange
        $this->loadSimpleDataset();
        $this->net->addInputLayer(2)
            ->addLayer(1, ActivationFunctions::RECTIFIER)
            ->addOutputLayer(1, ActivationFunctions::RECTIFIER)
            ->setWeights(0.1)
            ->setBiases(0.1);

        // Act
        $results = $this->algorithm->run();

        // Assert
        $this->assertEquals(1, sizeof($results));
        $this->assertEquals(0.116, $results[0]);
    }

    public function test_run_withSigmoid()
    {
        // Arrange
        $this->loadSimpleDataset();
        $this->net->addInputLayer(1)
            ->addLayer(1, ActivationFunctions::RECTIFIER)
            ->addOutputLayer(1, ActivationFunctions::SIGMOID)
            ->setWeights(0.1)
            ->setBiases(0.1);

        // Act
        $results = $this->algorithm->run();

        // Assert
        $expected = $this->activationFunctions->sigmoid(0.114);
        $this->assertEquals($expected, $results[0]);
    }

    public function test_runTest_accuracyShouldBe100()
    {
        // Arrange
        $this->loadAccuracyDataset100();
        $this->dataset->extract_test_samples(100);
        $this->net->addInputLayer(1)
            ->addLayer(1, ActivationFunctions::RECTIFIER)
            ->addOutputLayer(1, ActivationFunctions::RECTIFIER)
            ->setWeights(0.1)
            ->setBiases(0.1);

        // Act
        $accuracy = $this->algorithm->runTest();

        // Assert
        $this->assertEquals($accuracy, 100);
    }

    public function test_runTest_accuracyShouldBe50()
    {
        // Arrange
        $this->loadAccuracyDataset50();
        $this->dataset->extract_test_samples(100);
        $this->net->addInputLayer(1)
            ->addLayer(1, ActivationFunctions::RECTIFIER)
            ->addOutputLayer(1, ActivationFunctions::RECTIFIER)
            ->setWeights(0.1)
            ->setBiases(0.1);

        // Act
        $accuracy = $this->algorithm->runTest();

        // Assert
        $this->assertEquals($accuracy, 50);
    }

    public function test_run_nodesHaveCorrectOutput()
    {
        // Arrange
        $this->loadSimpleDataset();
        $this->net->addInputLayer(1)
            ->addLayer(1, ActivationFunctions::RECTIFIER)
            ->addOutputLayer(1, ActivationFunctions::RECTIFIER)
            ->setWeights(0.1)
            ->setBiases(0.1);

        // Act
        $this->algorithm->run();

        // Assert
        $this->assertEquals(0.14, $this->net->layers()[0]->nodes()[0]->output());
        $this->assertEquals(0.114, $this->net->outputLayer()->nodes()[0]->output());
    }
}