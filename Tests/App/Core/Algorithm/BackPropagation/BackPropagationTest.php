<?php

namespace Tests\App\Core\Algorithm\BackPropagation;

use App\Core\Activation\ActivationFunctions;
use App\Core\Algorithm\BackPropagation\BackPropagationRunner;
use App\Core\Loss\LossFunctions;
use Tests\BaseTest;

class BackPropagationTest extends BaseTest
{
    /**
     * @var BackPropagationRunner
     */
    private $algorithm;

    public function setUp()
    {
        parent::setUp();
    }

    public function setUpBackPropagation(float $learningRate, int $batchSize, int $epochs)
    {
        $this->algorithm = new BackPropagationRunner($this->net, $this->dataset, LossFunctions::LOSS_FUNCTION_SQUARE, $learningRate, $batchSize, $epochs);
    }

    public function test_run_simplestBackPropagationWeightsAreUpdatedCorrectly()
    {
        // Arrange
        $this->setUpBackPropagation(1.0, 1, 1);
        $this->loadSimpleDataset();
        $this->net->addInputLayer(1)
            ->addLayer(1, ActivationFunctions::RECTIFIER)
            ->addOutputLayer(1, ActivationFunctions::RECTIFIER)
            ->setWeights(0.1)
            ->setBiases(0.1);

        // Act
        $this->algorithm->run();

        // Assert
        $this->assertEquals(0.14, $this->net->layers()[0]->nodes()[0]->output());
        $this->assertEquals(0.114, $this->net->outputLayer()->nodes()[0]->output());
        $this->assertEquals(0.21004, $this->net->outputLayer()->nodes()[0]->weightAt(0));
        $this->assertEquals(0.166036576, $this->net->layers()[0]->nodes()[0]->weightAt(0)); // stopped here
    }

    public function test_run_simplestBackPropagationBiasesAreUpdatedCorrectly()
    {
        // Arrange
        $this->setUpBackPropagation(1.0, 1, 1);
        $this->loadSimpleDataset();
        $this->net->addInputLayer(1)
            ->addLayer(1, ActivationFunctions::RECTIFIER)
            ->addOutputLayer(1, ActivationFunctions::RECTIFIER)
            ->setWeights(0.1)
            ->setBiases(0.1);

        // Act
        $this->algorithm->run();

        // Assert
        $this->assertEquals(0.14, $this->net->layers()[0]->nodes()[0]->output());
        $this->assertEquals(0.114, $this->net->outputLayer()->nodes()[0]->output());
        $this->assertEquals(0.886, $this->net->outputLayer()->nodes()[0]->bias(0));
        $this->assertEquals(0.1, $this->net->layers()[0]->nodes()[0]->bias(0)); // stopped here
    }
}