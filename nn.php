<?php

require __DIR__.'/vendor/autoload.php';

use App\Core\Activation\ActivationFunctions;
use App\Core\Loss\LossFunctions;
use App\Core\NeuralNet;

$nn = new NeuralNet();

// *** reading and formatting data ***

$nn->setData('dataset.csv')
    ->shuffleData()
    ->setInputs(3, 12)
    ->setOutputs(13)
    ->encodeInputTextToInteger([1, 2])
    ->convertToDummyVariables(1)
    ->scaleAll()
    ->extract_test_samples(20);

// *** building the neural network ***

$nn->build()
    ->addInputLayer(11)
    ->addLayer(6, ActivationFunctions::RECTIFIER)
    ->addOutputLayer(1, ActivationFunctions::SIGMOID)
    ->setWeights(0.1)
    ->setBiases(0.5);

// *** training ***

$nn->train(LossFunctions::LOSS_FUNCTION_SQUARE, 0.05, 1, 20);

// *** run ***

//$results = $nn->run();

//print_r($results);
