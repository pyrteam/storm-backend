<?php

namespace Config;

use App\Core\Activation\ActivationFunctions;

class ActivationExtension extends ActivationFunctions
{
    protected function register()
    {
        parent::register();
    }
}